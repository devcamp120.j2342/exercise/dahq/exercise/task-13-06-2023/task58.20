package com.devcamp.drinksapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.drinksapi.model.CDrink;

public interface CDrinkRepository extends JpaRepository<CDrink, Integer> {

}
