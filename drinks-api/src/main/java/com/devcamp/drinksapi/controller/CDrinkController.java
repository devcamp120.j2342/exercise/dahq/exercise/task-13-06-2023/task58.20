package com.devcamp.drinksapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drinksapi.model.CDrink;
import com.devcamp.drinksapi.repository.CDrinkRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CDrinkController {
    @Autowired
    CDrinkRepository cDrinkRepository;

    @GetMapping("/drinks")

    public ResponseEntity<List<CDrink>> getAllDrinks() {
        try {
            List<CDrink> cDrinks = new ArrayList<CDrink>();
            cDrinkRepository.findAll().forEach(cDrinks::add);
            return new ResponseEntity<>(cDrinks, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
